package ru.kolevatykh.spring.controller.rest;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.kolevatykh.spring.dto.TaskDTO;
import ru.kolevatykh.spring.exception.EmptyInputException;
import ru.kolevatykh.spring.exception.ProjectNotFoundException;
import ru.kolevatykh.spring.exception.UserNotFoundException;
import ru.kolevatykh.spring.model.Task;
import ru.kolevatykh.spring.service.TaskService;

import java.util.List;

@RestController
@RequestMapping(value = "/task", produces = MediaType.APPLICATION_JSON_VALUE)
public class RestTaskController {

    @NotNull
    private final TaskService taskService;

    @Autowired
    public RestTaskController(
            @NotNull final TaskService taskService
    ) {
        this.taskService = taskService;
    }

    @GetMapping("/list")
    @ResponseBody
    public List<TaskDTO> findAll() throws Exception {
        return taskService.getListTaskDTO(taskService.findAll());
    }

    @PostMapping(value = "/create", consumes = MediaType.APPLICATION_JSON_VALUE)
    public void createTask(
            @RequestBody @NotNull final TaskDTO task
    ) throws Exception, UserNotFoundException, ProjectNotFoundException {
        if (task.getUserId().isEmpty()) ResponseEntity.badRequest().body("User id is empty.");
        taskService.persist(taskService.getTaskEntity(task));
    }

    @DeleteMapping("/delete/{user_id}/{id}")
    public ResponseEntity<Object> deleteTask(
            @PathVariable("user_id") @NotNull final String user_id,
            @PathVariable("id") @NotNull final String id
    ) throws Exception {
        if (id.isEmpty()) return ResponseEntity.badRequest().body("Project id is empty.");
        if (user_id.isEmpty()) return ResponseEntity.badRequest().body("User id is empty.");
        taskService.remove(user_id, id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping(value = "/update/{user_id}/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TaskDTO> updateTask(
            @RequestBody @Nullable final TaskDTO task,
            @PathVariable("user_id") @NotNull final String user_id,
            @PathVariable("id") @NotNull final String id
    ) throws Exception {
        if (id.isEmpty()) return ResponseEntity.badRequest().build();
        if (user_id.isEmpty()) return ResponseEntity.badRequest().build();
        taskService.merge(taskService.getTaskEntity(task));
        @Nullable final Task updatedTask = taskService.findOneById(user_id, id);
        if (updatedTask == null) {
            return ResponseEntity.notFound().build();
        } else {
            return ResponseEntity.ok(taskService.getTaskDTO(updatedTask));
        }
    }

    @GetMapping(value = "/view/{user_id}/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TaskDTO> viewTask(
            @PathVariable(name = "user_id") @NotNull final String user_id,
            @PathVariable(name = "id") @NotNull final String id
    ) throws Exception, UserNotFoundException, EmptyInputException {
        if (id.isEmpty()) return ResponseEntity.badRequest().build();
        if (user_id.isEmpty()) return ResponseEntity.badRequest().build();
        @Nullable final Task task = taskService.findOneById(user_id, id);
        if (task == null) {
            return ResponseEntity.notFound().build();
        } else {
            return ResponseEntity.ok(taskService.getTaskDTO(task));
        }
    }
}
