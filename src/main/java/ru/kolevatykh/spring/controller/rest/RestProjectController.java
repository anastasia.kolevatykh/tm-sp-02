package ru.kolevatykh.spring.controller.rest;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.kolevatykh.spring.dto.ProjectDTO;
import ru.kolevatykh.spring.exception.EmptyInputException;
import ru.kolevatykh.spring.exception.ProjectNotFoundException;
import ru.kolevatykh.spring.exception.UserNotFoundException;
import ru.kolevatykh.spring.model.Project;
import ru.kolevatykh.spring.service.ProjectService;

import java.util.List;

@RestController
@RequestMapping(value = "/project", produces = MediaType.APPLICATION_JSON_VALUE)
public class RestProjectController {

    @NotNull
    private final ProjectService projectService;

    @Autowired
    public RestProjectController(
            @NotNull final ProjectService projectService
    ) throws Exception, EmptyInputException, ProjectNotFoundException {
        this.projectService = projectService;
    }

    @GetMapping(value = "/list", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ProjectDTO> findAll() throws Exception {
        return projectService.getListProjectDTO(projectService.findAll());
    }

    @PostMapping(value = "/create", consumes = MediaType.APPLICATION_JSON_VALUE)
    public void createProject(
            @RequestBody @NotNull final ProjectDTO project
    ) throws Exception, UserNotFoundException, ProjectNotFoundException {
        if (project.getUserId().isEmpty()) ResponseEntity.badRequest().body("User id is empty.");
        projectService.persist(projectService.getProjectEntity(project));
    }

    @DeleteMapping("/delete/{user_id}/{id}")
    public ResponseEntity<Object> deleteProject(
            @PathVariable("user_id") @NotNull final String user_id,
            @PathVariable("id") @NotNull final String id
    ) throws Exception {
        if (id.isEmpty()) return ResponseEntity.badRequest().body("Project id is empty.");
        if (user_id.isEmpty()) return ResponseEntity.badRequest().body("User id is empty.");
        projectService.remove(user_id, id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/update/{user_id}/{id}")
    public ResponseEntity<ProjectDTO> updateProject(
            @RequestBody @Nullable final ProjectDTO project,
            @PathVariable("user_id") @NotNull final String user_id,
            @PathVariable("id") @NotNull final String id
    ) throws Exception {
        if (id.isEmpty()) return ResponseEntity.badRequest().build();
        if (user_id.isEmpty()) return ResponseEntity.badRequest().build();
        projectService.merge(projectService.getProjectEntity(project));
        @Nullable final Project updatedProject = projectService.findOneById(user_id, id);
        if (updatedProject == null) {
            return ResponseEntity.notFound().build();
        } else {
            return ResponseEntity.ok(projectService.getProjectDTO(updatedProject));
        }
    }

    @GetMapping("/view/{user_id}/{id}")
    public ResponseEntity<ProjectDTO> viewProject(
            @PathVariable(name = "user_id") @NotNull final String user_id,
            @PathVariable(name = "id") @NotNull final String id
    ) throws Exception, UserNotFoundException, EmptyInputException {
        if (id.isEmpty()) return ResponseEntity.badRequest().build();
        if (user_id.isEmpty()) return ResponseEntity.badRequest().build();
        @Nullable final Project project = projectService.findOneById(user_id, id);
        if (project == null) {
            return ResponseEntity.notFound().build();
        } else {
            return ResponseEntity.ok(projectService.getProjectDTO(project));
        }
    }
}
